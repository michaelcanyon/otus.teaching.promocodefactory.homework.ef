﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerPreferenceRepostitory : EfRepository<CustomerPrefrence>, ICustomerPreferenceRepository
    {
        public CustomerPreferenceRepostitory(SqLiteContext context):base(context)
        {}

        public Task AddRange(IEnumerable<CustomerPrefrence> customerPrefrences)
        {
            _context.CustomerPrefrences.AddRange(customerPrefrences);
            return Task.CompletedTask;
        }

        public async Task DeleteRange(List<Guid> ids)
        {
            var customerPrefrences = await _dbSet.Where(cp => ids.Contains(cp.Id)).ToListAsync();
            _context.CustomerPrefrences.RemoveRange(customerPrefrences);
        }
    }
}
