﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IDbRepository<T>
        where T : BaseEntity
    {

        protected SqLiteContext _context;
        protected DbSet<T> _dbSet;

        public EfRepository(SqLiteContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public IQueryable<T> GetAll()
            => _context.Set<T>();

        public Task<T> GetByIdAsync(Guid id)
            => _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public Guid Create(T model)
        {
            _dbSet.Add(model);
            return model.Id;
        }

        public T Update(T model)
        {
            _dbSet.Update(model);
            return model;
        }

        public void Delete(Guid id)
        {
            var entity = _dbSet.FirstOrDefault(x => x.Id == id);
            if (entity == null)
                throw new Exception("Сущность с указанным Id не найдена");

            _dbSet.Remove(entity);
        }

        public Task SaveChangesAsync()
            => _context.SaveChangesAsync();
    }
}
