﻿
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class SqLiteContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPrefrence> CustomerPrefrences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public SqLiteContext(DbContextOptions<SqLiteContext> options) : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPrefrence>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Promocodes)
                .WithOne(p => p.Customer)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CustomerPrefrence>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPrefrences)
                .HasForeignKey(cp => cp.CustomerId);

            modelBuilder.Entity<CustomerPrefrence>()
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPrefrences)
                .HasForeignKey(cp => cp.PreferenceId);
        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            base.ConfigureConventions(configurationBuilder);

            configurationBuilder.Properties<string>()
                .HaveMaxLength(300);
        }
    }
}
