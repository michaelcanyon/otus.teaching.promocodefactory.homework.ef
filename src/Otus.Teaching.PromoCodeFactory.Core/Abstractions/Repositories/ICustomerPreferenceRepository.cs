﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerPreferenceRepository: IDbRepository<CustomerPrefrence>
    {
        public Task DeleteRange(List<Guid> ids);

        public Task AddRange(IEnumerable<CustomerPrefrence> customerPrefrences);
    }
}
