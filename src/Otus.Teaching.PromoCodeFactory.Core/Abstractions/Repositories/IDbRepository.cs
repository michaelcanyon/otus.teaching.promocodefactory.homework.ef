﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IDbRepository<T>
        where T : BaseEntity
    {
        IQueryable<T> GetAll();

        Task<T> GetByIdAsync(Guid id);

        Guid Create(T model);

        T Update(T model);

        void Delete(Guid id);

        Task SaveChangesAsync();
    }
}
