﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPrefrence : BaseEntity
    {
        public Guid CustomerId { get; set; }

        public Guid PreferenceId { get; set; }

        public Customer Customer { get; set; }

        public Preference Preference { get; set; }
    }
}
