﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.SeedingData
{
    public static class FakeDataSeeding
    {
        public static IApplicationBuilder EnsureCreated(this IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.CreateScope();
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<SqLiteContext>();
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            return app;
        }

        public static IApplicationBuilder SeedEntity<T>(this IApplicationBuilder app, IEnumerable<T> entities) where T : BaseEntity
        {
            using var scope = app.ApplicationServices.CreateScope();
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<SqLiteContext>();
            context.Set<T>().AddRange(entities);
            context.SaveChanges();
            return app;
        }
    }
}
