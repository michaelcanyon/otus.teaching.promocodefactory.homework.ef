﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IDbRepository<Customer> _customerRepository;
        private readonly IPromocodeRepository _promoCodeRepository;
        private readonly ICustomerPreferenceRepository _customerPreferenceRepository;

        public CustomersController(IDbRepository<Customer> customerRepository,
                                   IPromocodeRepository promoCodeRepository,
                                   ICustomerPreferenceRepository customerPreferenceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var response = _customerRepository.GetAll();
            return Ok(await response
                .Select(c => new CustomerShortResponse()
                {
                    Id = c.Id,
                    Email = c.Email,
                    FirstName = c.FirstName,
                    LastName = c.LastName
                }).ToListAsync());
        }

        /// <summary>
        /// Получить клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var response = await _customerRepository
                .GetAll()
                .Include(c => c.Promocodes)
                .Include(c => c.CustomerPrefrences)
                .ThenInclude(cp => cp.Preference)
                .FirstOrDefaultAsync(c => c.Id == id);
            if (response == null)
                throw new Exception("Клиен с указанным идентификатором не найден");
            return Ok(new CustomerResponse()
            {
                Id = response.Id,
                Email = response.Email,
                FirstName = response.FirstName,
                LastName = response.LastName,
                PurchaseCount = response.PurchaseCount,
                PromoCodes = response.Promocodes?.Select(p =>
                    new PromoCodeShortResponse()
                    {
                        Id = p.Id,
                        BeginDate = p.BeginDate.ToString(),
                        EndDate = p.EndDate.ToString(),
                        Code = p.Code,
                        PartnerName = p.PartnerName,
                        ServiceInfo = p.ServiceInfo
                    }).ToList(),
                PreferenceResponses = response
                    .CustomerPrefrences?
                    .Select(cp =>
                        new PreferenceResponse
                        {
                            Id = cp.Preference.Id,
                            Name = cp.Preference.Name
                        })
            });
        }

        /// <summary>
        /// Создать запись клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            var id = Guid.NewGuid();
            var result = _customerRepository.Create(new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PurchaseCount = request.PurchaseCount,
                CustomerPrefrences = request.PreferenceIds.Select(p =>
                    new CustomerPrefrence
                    {
                        Id= Guid.NewGuid(),
                        CustomerId = id,
                        PreferenceId = p
                    }).ToList(),
                Promocodes = new List<PromoCode>()
            });
            await _customerRepository.SaveChangesAsync();
            return Ok(result);
        }

        /// <summary>
        /// Редактировать запись клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var customer = await _customerRepository.GetByIdAsync(id);
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.PurchaseCount = request.PurchaseCount;
            await _customerPreferenceRepository
                .DeleteRange(_customerPreferenceRepository.GetAll()
                                                          .Where(cp => id == cp.CustomerId)
                                                          .Select(cp => cp.Id)
                                                          .ToList());
            customer.CustomerPrefrences = request.PreferenceIds.Select(p => new CustomerPrefrence { Id = Guid.NewGuid(), CustomerId = id, PreferenceId = p })
                                                               .ToList();
            _customerRepository.Update(customer);

            await _customerRepository.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Удалить запись клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpDelete]
        public async Task DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            _customerRepository.Delete(id);
            await _customerRepository.SaveChangesAsync();
        }
    }
}