﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IDbRepository<Preference> _preferencRepository;

        public PreferenceController(IDbRepository<Preference> preferenceRepository)
        {
            _preferencRepository = preferenceRepository;
        }

        /// <summary>
        /// Возващает список всех заведенных предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetAll()
            => Ok(await _preferencRepository.GetAll()
                                            .Select(p =>
                                                new PreferenceResponse
                                                {
                                                    Id = p.Id,
                                                    Name = p.Name
                                                })
                                            .ToListAsync());

        /// <summary>
        /// Добавление предпочтения
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> Create(string title)
        {
            var response = _preferencRepository.Create(
                new Preference
                {
                    Id = Guid.NewGuid(),
                    Name = title
                });
            await _preferencRepository.SaveChangesAsync();
            return Ok(response);
        }

    }
}
