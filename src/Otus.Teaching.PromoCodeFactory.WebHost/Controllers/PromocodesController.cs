﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IDbRepository<PromoCode> _promocodeRepository;
        private readonly IDbRepository<Preference> _preferenceRepository;
        public PromocodesController(IDbRepository<PromoCode> promocodeRepository, IDbRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;   
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPromocodesAsync")]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
            => await _promocodeRepository
            .GetAll()
            .Select(pc =>
                new PromoCodeShortResponse
                {
                    Id = pc.Id,
                    Code = pc.Code,
                    PartnerName = pc.PartnerName,
                    ServiceInfo = pc.ServiceInfo,
                    BeginDate = pc.BeginDate.ToLocalTime().ToString(),
                    EndDate = pc.EndDate.ToLocalTime().ToString()
                })
            .ToListAsync();
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GivePromoCodesToCustomersWithPreferenceAsync")]
        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var lowerPreference= request.Preference.ToLower();
            var preference =await _preferenceRepository.GetAll()
                .Include(x=>x.CustomerPrefrences)
                .ThenInclude(x=>x.Customer)
                .ThenInclude(c=>c.Promocodes)
                .FirstOrDefaultAsync(pr => pr.Name.ToLower().Equals(lowerPreference));

             var customers =  preference
                .CustomerPrefrences
                .Select(cp => cp.Customer)
                .ToList();

            customers.ForEach(c =>_promocodeRepository.Create(new PromoCode 
                { 
                    Id = Guid.NewGuid(), 
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName, 
                    Code = request.PromoCode, 
                    Preference = preference, 
                    BeginDate = DateTime.UtcNow,
                    EndDate = DateTime.UtcNow.AddDays(15),
                    Customer=c
                }));
            await _promocodeRepository.SaveChangesAsync();
        }
    }
}